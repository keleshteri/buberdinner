# Buber Dinner API

- [Buber Dinner API](#buber-dinner-api)
  - [Auth](#auth)
    - [Register](#register)
      - [Register Request](#register-request)
      - [Register Response](#register-response)
    - [Login](#login)
      - [Login Request](#login-request)
      - [Login Response](#login-response)
  
## Auth

### Register
``` js
POST {{host}}/auth/register
```
 

#### Register Request
```js
{
    "firstName":"Mohammad Mehdi",
    "lastName":" Shaban Keleshteri",
    "email":"mehdi.shaban.keleshteri@outlook.com",
    "password":"Pass888"
}
```
#### Register Response
```js
200 OK
```
```js
"id":"d89c2d9a-eb3e-4075095ff-b920b55aa104",
    "firstName":"Mohammad Mehdi",
    "lastName":" Shaban Keleshteri",
    "email":"mehdi.shaban.keleshteri@outlook.com",
    "token":"eyjhb..hbbQ"
```

### Login
```js
POST {{host}}/auth/login
```

#### Login Request
```json
{
    "email":"mehdi.shaban.keleshteri@outlook.com",
    "password":"Pass888"
}
```
#### Login Response
```json
{
    "id":"d89c2d9a-eb3e-4075095ff-b920b55aa104",
    "firstName":"Mohammad Mehdi",
    "lastName":" Shaban Keleshteri",
    "email":"mehdi.shaban.keleshteri@outlook.com",
    "token":"eyjhb..hbbQ"
}
```